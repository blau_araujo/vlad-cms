<?php include "parsedown.php"; $Parsedown = new Parsedown(); ?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8" />
    <title>Teste do Parsedown</title>
    <style type="text/css">
        body { text-align:center; }
        .all {display:block; width:740px; text-align:left; margin:0 auto;}
    </style>
</head>
<body>
    <div class="all">
        <?php echo $Parsedown->text(file_get_contents('teste.md')); ?>    
    </div>
</body>
</html>

